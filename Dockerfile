FROM golang:1.15 as build

COPY ./ /app
RUN unset GOPATH && cd /app && go mod download && go build -o server

FROM golang:1.15

COPY --from=build /app/server /app/server

CMD /app/server