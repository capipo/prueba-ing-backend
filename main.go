package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/shopspring/decimal"
)

var levels = map[string]int{
	"A":     5,
	"B":     10,
	"C":     15,
	"Cuauh": 20,
}

type Player struct {
	Name        string `json:"nombre"`
	Level       string `json:"nivel"`
	Target      int    `json:"goles_minimos"`
	Goals       int    `json:"goles"`
	Salary      int    `json:"sueldo"`
	Bonus       int    `json:"bono"`
	TotalSalary float64  `json:"sueldo_completo"`
	Team        string `json:"equipo"`
}

func (p *Player) Totalize(teams map[string]*TeamScore) {
	team := teams[p.Team]
	factor := factor(team.Goals, team.Target).Add(factor(p.Goals, p.Target)).Div(decimal.NewFromInt(2))
	p.TotalSalary, _ = factor.
		Mul(decimal.NewFromInt(int64(p.Bonus))).
		Add(decimal.NewFromInt(int64(p.Salary))).
		Round(2).
		Float64()
}

type TeamScore struct {
	Name   string
	Goals  int
	Target int
}

func main() {
	http.HandleFunc("/", Handle)
	log.Fatal(http.ListenAndServe(":8081", nil))
}

func Handle(w http.ResponseWriter, r *http.Request) {
	body := struct {
		Players []*Player `json:"jugadores"`
	}{}
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&body); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	teams := map[string]*TeamScore{}
	for _, player := range body.Players {
		if _, ok := teams[player.Team]; !ok {
			teams[player.Team] = &TeamScore{player.Team, 0, 0}
		}
		player.Target = levels[player.Level]
		team := teams[player.Team]
		team.Goals += player.Goals
		team.Target += player.Target
	}

	for _, player := range body.Players {
		player.Totalize(teams)
	}

	res, _ := json.Marshal(body)
	fmt.Fprintf(w, string(res))
}

func factor(goals int, target int) decimal.Decimal {
	if goals > target {
		return decimal.NewFromInt(1)
	}
	return decimal.NewFromInt(int64(goals)).Div(decimal.NewFromInt(int64(target)))
}
