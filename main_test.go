package main

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

var postBody = `
{
    "jugadores" : [  
      {  
         "nombre":"Juan Perez",
         "nivel":"C",
         "goles":10,
         "sueldo":50000,
         "bono":25000,
         "sueldo_completo":null,
         "equipo":"rojo"
      },
      {  
         "nombre":"EL Cuauh",
         "nivel":"Cuauh",
         "goles":30,
         "sueldo":100000,
         "bono":30000,
         "sueldo_completo":null,
         "equipo":"azul"
      },
      {  
         "nombre":"Cosme Fulanito",
         "nivel":"A",
         "goles":7,
         "sueldo":20000,
         "bono":10000,
         "sueldo_completo":null,
         "equipo":"azul"

      },
      {  
         "nombre":"El Rulo",
         "nivel":"B",
         "goles":9,
         "sueldo":30000,
         "bono":15000,
         "sueldo_completo":null,
         "equipo":"rojo"

      }
   ]
}`

func TestServer(t *testing.T) {
	request := httptest.NewRequest(http.MethodPost, "/", strings.NewReader(postBody))
	rr := httptest.NewRecorder()

	http.HandlerFunc(Handle).ServeHTTP(rr, request)

	if rr.Code != http.StatusOK {
		t.Errorf("Want status '%d', got '%d'", http.StatusOK, rr.Code)
	}

	body := struct {
		Players []*Player `json:"jugadores"`
	}{}
	json.NewDecoder(rr.Body).Decode(&body)

	var rulo *Player = nil
	for _, player := range body.Players {
		if player.Name == "El Rulo" {
			rulo = player
			break
		}
	}

	if rulo == nil {
		t.Logf("El Rulo not found")
		t.FailNow()
	}
	if rulo.TotalSalary != 42450 {
		t.Errorf("Wrong TotalSalary, Want '%v', got '%v'", 42450, rulo.TotalSalary)
	}
}
