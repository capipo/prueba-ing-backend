## Ejecutar test

```sh
go test *.go
```

## Ejecutar servidor

```sh
go run main
```

Ahora se puede hacer un request a `localhost:8081`

## Utilizando docker

### Ejecutar tests

```sh
sudo docker run --rm -w /app -it -v $(pwd)/:/app golang:1.15 go test *.go
```

### Construir imagen de producción

```sh
sudo docker build -t myapp .
```

### Ejecutar servidor

```sh
sudo docker run --rm -it -p 8081:8081 myapp
```